package com.zuitt.example;

// Parent Class
public class Animal {
    // Properties
    protected String name;
    protected String color;

    // constructor
    public Animal(){}

    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }
    // setter and getter
    public String getName(){
        return this.name;
    }
    public String getColor(){
        return this.color;
    }
    // Setter
    public void setName(String name){
        this.name = name;
    }
    public void setColor(String color){
        this.color = color;
    }
    // Method
    public void call(){
        System.out.println("Hi my name is " + this.name);
    }
}
